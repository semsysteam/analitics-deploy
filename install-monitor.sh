#!/usr/bin/env bash
set -eu pipefail

#
# Скрипт установки сервисов мониторинга
#

VERBOSE=${VERBOSE:-}
if [[ ${VERBOSE} != "" ]]; then
  set -x
fi

if [[ $EUID = 0 ]]; then
  printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
  exit 1
fi

_check_internet_conection() {
  echo -e "[ * ] Проверка подключения к сети Интернет ..."
  sleep 1
  echo -e "GET https://google.com HTTP/1.0\n\n" | nc google.com 443 >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo -e "[ X ] Сеть Интернет ➜ НЕ ПОДКЛЮЧЕНА!\n"
    echo -e "Извините, но для продолжения установки необходимо подключение к сети Интернет ...."
    exit 0
  else
    echo -e "[ ✔ ] Сеть Интернет ➜  ПОДКЛЮЧЕНА!\n"
    sleep 1
  fi
}

CURRENT_DIR=$(pwd)

# shellcheck disable=SC1091
. .env-install

# Настройка каталогов и установка системных утилит
_do_install_min() {
  if ! [ -d "$VOLUMES" ]; then
    mkdir -p "$VOLUMES"
  fi
}

_do_create_db() {

  echo -e "[ * ] Создание БД ..."

  # создаем пользователя $TIMESCALE_USER
  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
CREATE ROLE $TIMESCALE_USER WITH LOGIN SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION PASSWORD '$TIMESCALE_PASSWORD';
EOF

  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
CREATE DATABASE $TIMESCALE_DB WITH OWNER = $TIMESCALE_USER CONNECTION LIMIT = -1;
EOF

  echo -e "[ ✔ ] Создание БД ➜  ЗАВЕРШЕНО!\n"
}

_do_copy_files() {

  echo -e "[ * ] Копирование настроек для grafana promscale prometheus ..."

  cp -r "$CURRENT_DIR"/dists/monitor "$VOLUMES"

  printf '
GF_INSTALL_PLUGINS=grafana-clock-panel,briangann-gauge-panel,natel-plotly-panel,grafana-simple-json-datasource,marcusolsson-json-datasource,natel-discrete-panel,agenty-flowcharting-panel,marcusolsson-gantt-panel,marcusolsson-hourly-heatmap-panel,flant-statusmap-panel
GF_PLUGINS_ALLOW_LOADING_UNSIGNED_PLUGINS=natel-discrete-panel,agenty-flowcharting-panel,marcusolsson-gantt-panel,marcusolsson-hourly-heatmap-panel,flant-statusmap-panel,marcusolsson-json-datasource
GF_SECURITY_ADMIN_USER=admin
GF_SECURITY_ADMIN_PASSWORD=admin
POSTGRES_VERSION='"$POSTGRES_VERSION"'
POSTGRES_HOST='"$POSTGRES_HOST"'
POSTGRES_PORT='"$POSTGRES_PORT"'
TIMESCALE_DB='"$TIMESCALE_DB"'
TIMESCALE_USER='"$TIMESCALE_USER"'
TIMESCALE_PASSWORD='"$TIMESCALE_PASSWORD"'\n' >"$VOLUMES"/monitor/.env
  
  cd "$VOLUMES"/monitor
  docker-compose up -d --build --remove-orphans
  sudo chown -R 472:472 grafana/
  cd "$CURRENT_DIR"
  sleep 1
  echo -e "[ ✔ ] Копирование настроек для grafana promscale prometheus ➜  ЗАВЕРШЕНО!\n"
}

_main() {

  _check_internet_conection

  if [[ ${EXTERNAL_ADDRESS} != "" ]]; then
    if [[ ${POSTGRES_HOST} != "" ]] && [[ ${POSTGRES_PORT} != "" ]] && [[ ${TIMESCALE_DB} != "" ]] && [[ ${TIMESCALE_USER} != "" ]] && [[ ${TIMESCALE_PASSWORD} != "" ]]; then
      echo -e "[ * ] Установка сервисов grafana promscale prometheus ..."      
      _do_install_min
      _do_create_db
      _do_copy_files
      echo -e "[ ✔ ] Установка grafana promscale prometheus ➜  ЗАВЕРШЕНО!\n"
      
    else
      printf '%s\n' "Одна или больше переменных в файле конфгурации .env-install не заполнены!" >&2
      exit 1
    fi
  else
    printf '%s\n' "Переменная EXTERNAL_ADDRESS не заполнена" >&2
    exit 1
  fi

}

_main
