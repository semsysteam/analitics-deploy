#!/usr/bin/env bash
set -eu pipefail

# Проверка сущестования команды
command_exists() {
  command -v "$@" >/dev/null 2>&1
}

not_exists=0

_main() {

  if command_exists docker && [ -e /var/run/docker.sock ]; then
    echo -e "[ * ] Удаление docker ..."
    sudo apt-get remove -y docker-ce-cli docker-ce
    sudo apt-get purge -y docker-ce-cli docker-ce
    sudo apt-get clean && sudo apt-get autoremove -y -qq

    if [ -f /etc/apt/sources.list.d/docker.list ]; then
      sudo rm -rf /etc/apt/sources.list.d/docker.*
    fi

    sudo rm -rf /var/lib/docker /etc/docker
    sudo rm -rf /etc/apparmor.d/docker
    sudo rm -rf /var/run/docker.sock
    sudo rm -rf /usr/bin/docker-compose

    if grep -q docker /etc/group; then
      sudo groupdel docker
    fi

    if [ -d "$HOME"/.docker ]; then
      sudo rm -rf "$HOME"/.docker
    fi

    echo -e "[ ✔ ] Удаление docker ➜ ЗАВЕРШЕНО!\n"

  else
    not_exists=1

    echo -e "[ X ] ВНИМАНИЕ: docker не установлен в вашей системе.!\n"
  fi

  if command_exists docker-compose; then
    echo -e "[ * ] Удаление docker-compose ..."

    sudo rm -f "$(which docker-compose)"

    sleep 1

    echo -e "[ ✔ ] Удаление docker-compose ➜ ЗАВЕРШЕНО!\n"

  else
    not_exists=1

    echo -e "[ X ] ВНИМАНИЕ: docker-compose не установлен в вашей системе.!\n"
  fi

  if [ $not_exists -eq 0 ]; then
    echo -e "[ ! ] ВНИМАНИЕ: Выполните перезагрузку сервера.!\n"
  else
    echo -e "[ ! ] ВНИМАНИЕ: Нет данных для удаления!\n"
  fi

}

_main
