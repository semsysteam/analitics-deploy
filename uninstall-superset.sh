#!/usr/bin/env bash
set -eu pipefail

CURRENT_DIR=$(pwd)

# shellcheck disable=SC1091
. .env-install

SUPERSET_PATH="$VOLUMES"/superset

# Проверка сущестования команды
command_exists() {
  command -v "$@" >/dev/null 2>&1
}

# Удаление БД и роли
_drop_role_and_db() {

  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
DROP DATABASE IF EXISTS $SUPERSET_DB;
EOF

  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
DROP OWNED BY $SUPERSET_USER;
EOF

  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
DROP ROLE IF EXISTS $SUPERSET_USER;
EOF

}

_main() {
  if [[ ${EXTERNAL_ADDRESS} != "" ]]; then
    if command_exists docker-compose; then
      echo -e "[ * ] Удаление superset ..."
      if [ -d "$SUPERSET_PATH" ]; then
        cd "$SUPERSET_PATH"
        docker-compose down --volumes --remove-orphans

        cd "$CURRENT_DIR"
        sudo rm -rf "$SUPERSET_PATH"
      fi

      if command_exists psql; then
        _drop_role_and_db
      fi

      echo -e "[ ✔ ] Удаление superset ➜  ЗАВЕРШЕНО!\n"

    else
      cat >&2 <<-'EOF'
	  ОШИБКА: docker-compose не установлен в вашей системе.
	EOF
      exit 1
    fi
  else
    cat >&2 <<-'EOF'
	  ОШИБКА: переменная EXTERNAL_ADDRESS не заполнена.
	EOF
    exit 1
  fi
}

_main
