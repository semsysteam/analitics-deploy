#!/usr/bin/env bash
set -eu pipefail

#
# Скрипт установки superset
#

VERBOSE=${VERBOSE:-}
if [[ ${VERBOSE} != "" ]]; then
  set -x
fi

if [[ $EUID = 0 ]]; then
  printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
  exit 1
fi

CURRENT_DIR=$(pwd)

# shellcheck disable=SC1091
. .env-install

SUPERSET_PATH="$VOLUMES"/superset
env_docker_file="$SUPERSET_PATH"/docker/.env-non-dev

_check_internet_conection() {
  echo -e "[ * ] Проверка подключения к сети Интернет ..."
  sleep 1
  echo -e "GET https://google.com HTTP/1.0\n\n" | nc google.com 443 >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo -e "[ X ] Сеть Интернет ➜ НЕ ПОДКЛЮЧЕНА!\n"
    echo -e "Извините, но для продолжения установки необходимо подключение к сети Интернет ...."
    exit 0
  else
    echo -e "[ ✔ ] Сеть Интернет ➜  ПОДКЛЮЧЕНА!\n"
    sleep 1
  fi
}

# Настройка каталогов и установка системных утилит
_do_install_min() {
  if ! [ -d "$VOLUMES" ]; then
    mkdir -p "$VOLUMES"
  fi
}

_do_create_db() {

  echo -e "[ * ] Создание БД ..."

  # создаем пользователя $SUPERSET_USER
  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
CREATE ROLE $SUPERSET_USER WITH LOGIN SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION PASSWORD '$SUPERSET_PASSWORD';
EOF

  # создаем БД $SUPERSET_DB
  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
CREATE DATABASE $SUPERSET_DB WITH OWNER = $SUPERSET_USER CONNECTION LIMIT = -1;
EOF

  echo -e "[ ✔ ] Создание БД ➜  ЗАВЕРШЕНО!\n"
}

_do_copy_files() {

  echo -e "[ * ] Копирование superset из удаленного репозитория ..."

  cd "$VOLUMES"

  if [ -d "$SUPERSET_PATH" ]; then
    sudo rm -fr "$SUPERSET_PATH"
  fi

  git clone https://github.com/apache/superset.git

  echo -e "[ ✔ ] Копирование superset ➜  ЗАВЕРШЕНО!\n"

  echo -e "[ * ] Применение настроек superset ..."

  sudo apt-get -qq update
  sudo apt-get install -y -qq build-essential jq libssl-dev libffi-dev python-dev python-pip libsasl2-dev libldap2-dev python3-babel >/dev/null 2>&1

  if ! [ -f "$env_docker_file" ]; then
    cp -vf "$CURRENT_DIR"/dists/superset/.env-non-dev "$env_docker_file"
  fi

  echo "$POSTGRES_HOST" | sed -i 's/DATABASE_HOST=.*/DATABASE_HOST='"$POSTGRES_HOST"'/g' "$SUPERSET_PATH"/docker/.env-non-dev
  echo "$POSTGRES_PORT" | sed -i 's/DATABASE_PORT=.*/DATABASE_PORT='"$POSTGRES_PORT"'/g' "$SUPERSET_PATH"/docker/.env-non-dev
  sed -i 's/SUPERSET_LOAD_EXAMPLES=.*/SUPERSET_LOAD_EXAMPLES=no/g' "$SUPERSET_PATH"/docker/.env-non-dev

  rm -f "$SUPERSET_PATH"/docker-compose.yml
  rm -f "$SUPERSET_PATH"/docker-compose-non-dev.yml

  cp -vf "$CURRENT_DIR"/dists/superset/docker-compose.yml "$SUPERSET_PATH"/docker-compose.yml
  cp -vf "$CURRENT_DIR"/dists/superset/superset_config_docker.py "$SUPERSET_PATH"/docker/pythonpath_dev/superset_config_docker.py
  cp -avrf "$CURRENT_DIR"/dists/superset/translations/ru/LC_MESSAGES/ "$SUPERSET_PATH"/superset/translations/ru/LC_MESSAGES/

  cd "$CURRENT_DIR"
  sleep 1
  echo -e "[ ✔ ] Применение настроек superset ➜  ЗАВЕРШЕНО!\n"

}

_main() {

  _check_internet_conection

  if [[ ${EXTERNAL_ADDRESS} != "" ]]; then
    if [[ ${POSTGRES_HOST} != "" ]] && [[ ${POSTGRES_PORT} != "" ]] && [[ ${SUPERSET_DB} != "" ]] && [[ ${SUPERSET_USER} != "" ]] && [[ ${SUPERSET_PASSWORD} != "" ]]; then
      echo -e "[ * ] Установка сервисов superset ..."
      _do_install_min
      _do_create_db
      _do_copy_files
      echo -e "[ ✔ ] Установка сервисов ➜  ЗАВЕРШЕНО!\n"
    else
      printf '%s\n' "Одна или больше переменных в файле конфгурации .env-install не заполнены!" >&2
      exit 1
    fi
  else
    printf '%s\n' "Переменная EXTERNAL_ADDRESS не заполнена" >&2
    exit 1
  fi

}

_main
