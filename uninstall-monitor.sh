#!/usr/bin/env bash
set -eu pipefail

# shellcheck disable=SC1091
. .env-install

CURRENT_DIR=$(pwd)
MONITOR_PATH="$VOLUMES"/monitor

# Проверка сущестования команды
command_exists() {
  command -v "$@" >/dev/null 2>&1
}

# Удаление БД и роли
_drop_role_and_db() {
  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
DROP DATABASE IF EXISTS $TIMESCALE_DB;
EOF

  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
DROP OWNED BY $TIMESCALE_USER;
EOF

  sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
DROP ROLE IF EXISTS $TIMESCALE_USER;
EOF

}

_main() {
  
  if [[ ${EXTERNAL_ADDRESS} != "" ]]; then
    if command_exists docker-compose; then

      echo -e "[ * ] Удаление grafana promscale prometheus ..."
      
      if [ -d "$MONITOR_PATH" ]; then
        cd "$MONITOR_PATH"
        docker-compose down --volumes --remove-orphans
        cd "$CURRENT_DIR"
        sudo rm -rf "$MONITOR_PATH"
      fi

      if command_exists psql; then
        _drop_role_and_db
      fi
      sleep 1
      echo -e "[ ✔ ] Удаление grafana promscale prometheus ➜  ЗАВЕРШЕНО!\n"
      
    else
      cat >&2 <<-'EOF'
		  ОШИБКА: docker-compose не установлен в вашей системе.
		EOF
      exit 1
    fi
  else
    printf '%s\n' "ОШИБКА: Переменная EXTERNAL_ADDRESS не заполнена" >&2
    exit 1
  fi

}

_main
