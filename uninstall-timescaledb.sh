#!/usr/bin/env bash
set -eu pipefail

#
# Скрипт удаления timescaledb
#

VERBOSE=${VERBOSE:-}
if [[ ${VERBOSE} != "" ]]; then
  set -x
fi

# shellcheck disable=SC1091
. .env-install

# Проверка сущестования команды
command_exists() {
  command -v "$@" >/dev/null 2>&1
}

_main() {

  if command_exists psql; then    
    echo -e "[ * ] Удаление timescaledb ..."
    sudo DEBIAN_FRONTEND=noninteractive apt-get remove -y timescaledb-2* postgresql*

    echo -e "[ * ] Очистка системы после удаления ..."

    sudo apt-get purge -y timescaledb-2* postgresql*
    sudo apt-get autoremove -y --purge timescaledb-2* postgresql*
    sudo apt-get autoclean

    sudo add-apt-repository --remove ppa:timescale/timescaledb-ppa -y

    if [ -f /etc/apt/sources.list.d/pgdg.list ]; then
      sudo rm -rf /etc/apt/sources.list.d/pgdg.*
    fi
    if [ -d /etc/postgresql-common ]; then
      sudo rm -rf /etc/postgresql-common
    fi
    if [ -d /etc/postgresql ]; then
      sudo rm -rf /etc/postgresql
    fi
    if [ -f /var/log/postgresql ]; then
      sudo rm -rf /var/log/postgresql
    fi
    if [ -d /var/lib/postgresql ]; then
      sudo rm -rf /var/lib/postgresql
    fi

    sudo userdel -f postgres

    echo -e "[ ✔ ] Очистка системы после удаления ➜ ЗАВЕРШЕНО!\n"
    sleep 1
    echo -e "[ ✔ ] Удаление timescaledb ➜ ЗАВЕРШЕНО!\n"
    exit 0
  
  else
    cat >&2 <<-'EOF'
		  ВНИМАНИЕ: timescaledb не установлен в вашей системе.
		EOF
    exit 1
  fi

}

_main
