# Changelog

## 0.3.1 (2021-02-02)

### Bug fixes

- Reduced precision when using strings as time ([#5](https://github.com/marcusolsson/grafana-gantt-panel/issues/5))

## 0.3.0 (2021-01-28)

### Enhancements

- Null values are interpreted as infinity
- Add duration to tooltip
- Improved styles

## 0.2.0 (2020-01-27)

### Enhancements

- Allow using string and number fields as time ([#1](https://github.com/marcusolsson/grafana-gantt-panel/issues/1), [#2](https://github.com/marcusolsson/grafana-gantt-panel/issues/2))

## 0.1.0 (2020-01-08)

Initial release. Not fit for production use.
