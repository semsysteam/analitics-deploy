#!/usr/bin/env bash

#
# Скрипт установки docker и docker-compose
#

set -eu pipefail

VERBOSE=${VERBOSE:-}
if [[ ${VERBOSE} != "" ]]; then
  set -x
fi

if [[ $EUID = 0 ]]; then
  printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
  exit 1
fi

failed=0

# Добавляем текущего пользователя
echo "$USER ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/dont-prompt-"$$SER"-for-sudo-password

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

_check_internet_conection() {
  echo -e "[ * ] Проверка подключения к сети Интернет"
  sleep 1
  echo -e "GET https://google.com HTTP/1.0\n\n" | nc google.com 443 >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo -e "[ X ] Сеть Интернет ➜ НЕ ПОДКЛЮЧЕНА!\n"
    echo -e "Извините, но для продолжения установки необходимо подключение к сети Интернет ...."
    failed=1
    exit 0
  else
    echo -e "[ ✔ ] Сеть Интернет ➜  ПОДКЛЮЧЕНА!\n"
    failed=0
    sleep 1
  fi
}

# Установка docker и docker-compose
_do_install_docker() {

  if ! command_exists docker && ! [ -e /var/run/docker.sock ]; then
    echo -e "[ * ] Установка и настройка docker..."
    sleep 1
    sudo apt-get -y -qq remove docker docker-engine docker.io containerd runc
    sudo apt-get -qq update
    sudo apt-get install -y -qq build-essential libssl-dev libffi-dev python-dev python-pip libsasl2-dev libldap2-dev apt-transport-https ca-certificates curl gnupg-agent software-properties-common screen htop git p7zip rdiff-backup >/dev/null 2>&1

    curl -fsSL https://get.docker.com -o get-docker.sh

    sh get-docker.sh
    echo -e "[ ✔ ] Установка docker ➜ ЗАВЕРШЕНА!\n"
    rm -f get-docker.sh
    echo -e "[ * ] Настройка docker..."

    # Включаем автозапуск
    sudo systemctl enable --now containerd || exit 1
    sudo systemctl enable --now docker || exit 1
    sudo systemctl start containerd || exit 1
    sudo systemctl start docker || exit 1

    [ "$(getent group docker)" ] || groupadd docker

    # Добавляем пользователя в группу docker
    sudo usermod -aG docker "$USER"

    #/usr/bin/newgrp вызывается явно, чтобы избежать неявного exec
    echo
    /usr/bin/newgrp docker <<EONG
    id
EONG

    echo -e "[ ✔ ] Настройка docker ➜ ЗАВЕРШЕНА!\n"
    sleep 1
    echo -e "[ ✔ ] установка и настройка docker ➜ ЗАВЕРШЕНА!\n"
    failed=0
  else
    sleep 1
    failed=1
    echo -e "[ ! ] docker уже установлен в вашей системе ➜ ПРОПУСКАЕМ!\n"
  fi

  if ! command_exists docker-compose; then
    echo -e "[ * ] Установка docker-compose ..."

    # получаем последний выпущенный тег сборки docker-compose
    COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)

    # скачиваем и установливаем docker-compose
    curl -L https://github.com/docker/compose/releases/download/"${COMPOSE_VERSION}"/docker-compose-"$(uname -s)"-"$(uname -m)" >"$HOME"/docker-compose
    sudo mv -u "$HOME"/docker-compose /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    curl -L https://raw.githubusercontent.com/docker/compose/"${COMPOSE_VERSION}"/contrib/completion/bash/docker-compose >"$HOME"/docker-compose-completion
    sudo mv -u "$HOME"/docker-compose-completion /etc/bash_completion.d/docker-compose
    echo -e "[ ✔ ] установка docker-compose ➜ ЗАВЕРШЕНА!\n"
    failed=0
  else
    sleep 1
    failed=1
    echo -e "[ ! ] docker-compose уже установлен в вашей системе ➜ ПРОПУСКАЕМ!\n"
  fi
}

_main() {

  _check_internet_conection
  _do_install_docker

  if [ $failed -eq 0 ]; then
    echo -e "[ ! ] ВНИМАНИЕ: Выполните перезагрузку сервера.!\n"
  fi

}

_main
