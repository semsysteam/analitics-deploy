#!/usr/bin/env bash
set -eu pipefail

#
# Скрипт установки timescaledb
#

VERBOSE=${VERBOSE:-}
if [[ ${VERBOSE} != "" ]]; then
  set -x
fi

failed=0

if [[ $EUID = 0 ]]; then
  printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
  failed=1
  exit 1
fi

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

CURRENT_DIR=$(pwd)

# shellcheck disable=SC1091
. .env-install

_check_internet_conection() {
  echo -e "[ * ] Проверка подключения к сети Интернет"
  sleep 1
  echo -e "GET https://google.com HTTP/1.0\n\n" | nc google.com 443 >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo -e "[ X ] Сеть Интернет ➜ НЕ ПОДКЛЮЧЕНА!\n"
    echo -e "Извините, но для продолжения установки необходимо подключение к сети Интернет ...."
    failed=1
    exit 0
  else
    echo -e "[ ✔ ] Сеть Интернет ➜  ПОДКЛЮЧЕНА!\n"
    failed=0
    sleep 1
  fi
}

# Установка timescaledb
_do_install_timescaledb() {

  if command_exists postgresql; then
    cat >&2 <<-'EOF'
		  ВНИМАНИЕ: timescaledb уже установлен в вашей системе.
		EOF
    exit 1
  else
    echo -e "[ * ] Установка и настройка timescaledb ..."
    sleep 1
    echo -e "[ * ] Установка timescaledb ..."
    echo -e "#########################################################"
    echo -e "# Ожидайте окончания выполнения скрита!                 #"
    echo -e "#########################################################"
    echo
    if [[ ${POSTGRES_VERSION} == "" ]]; then
      POSTGRES_VERSION=12
    fi
    # Создаем файл конфигурации репозитория:
    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    # Импортируем ключ подписи репозитория:
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    # Добавляем источник пакетов для timescaledb
    sudo add-apt-repository ppa:timescale/timescaledb-ppa -y
    # Обновляем список доступных пакетов
    sudo apt-get update -qq >/dev/null
    # Теперь установим пакет для версии $POSTGRES_VERSION
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install timescaledb-2-postgresql-"$POSTGRES_VERSION"

    echo
    echo -e "[ ✔ ] Установка timescaledb ➜  ЗАВЕРШЕНА!\n"
    sleep 1
    echo -e "[ * ] Настройка timescaledb ..."

    # Устанавливаем рекомендуемые настройки для timescaledb
    sudo timescaledb-tune --quiet --yes

    sudo service postgresql restart

    sudo cp -f "$CURRENT_DIR"/dists/pg_hba.conf /etc/postgresql/"$POSTGRES_VERSION"/main/pg_hba.conf
    sudo cp -f "$CURRENT_DIR"/dists/pg_custom.conf /etc/postgresql/"$POSTGRES_VERSION"/main/conf.d/pg_custom.conf

    sudo service postgresql restart
    echo -e "[ ✔ ] Настройка timescaledb ➜  ЗАВЕРШЕНА!\n"
    sleep 1
    echo -e "[ * ] Установка и настройка timescaledb ➜  ЗАВЕРШЕНА!\n"

  fi

}

_main() {
  _check_internet_conection
  _do_install_timescaledb
}

_main
